var grunt = require('grunt')
grunt.loadNpmTasks('grunt-mocha-test')

grunt.initConfig({
    mochaTest: {
      test: {
        options: {
          reporter: 'mocha-jenkins-reporter',
          reporterOptions: {
                "junit_report_name": "Tests",
                "junit_report_path": "results.xml",
                "junit_report_stack": 1
          }, // Optionally capture the reporter output to a file
          quiet: false, // Optionally suppress output to standard out (defaults to false)
          clearRequireCache: false // Optionally clear the require cache before running tests (defaults to false)
        },
        src: ['test/**/*.js']
      }
    }
})

grunt.registerTask('test', 'mochaTest')