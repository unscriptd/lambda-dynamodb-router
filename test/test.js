const chai = require('chai')
const sinon = require('sinon')

chai.should()
chai.use(require('sinon-chai'))

var _ = require('lodash')
var router = require('../lib/router')

describe('router', function() {
	let routes = {}

	const sns = {
		publish: sinon.stub().returnsThis(),
		promise: sinon.stub().resolves(),
	}

	const lambda = {
		invoke: sinon.stub().returnsThis(),
		promise: sinon.stub().resolves(),
	}
	beforeEach(() => {
		routes = {}
		sns.publish = sinon.stub().returnsThis()
		sns.promise = sinon.stub().resolves()
		lambda.invoke = sinon.stub().returnsThis()
		lambda.promise = sinon.stub().resolves()
	})

	it('should send an sns message to insert route only matching the criteria', async () => {
		var uuid = 'somelonguuidstring'

		var event = {
			Records: [
				{
					dynamodb: {
						Keys: {
							uuid: {
								S: uuid,
							},
						},
						NewImage: {
							key: {
								S: 'value',
							},
							uuid: {
								S: uuid,
							},
						},
					},
					eventSourceARN: 'arn:aws:dynamodb:us-west-2:account-id:table/test/stream/2015-06-27T00:48:05.899',
					eventName: 'INSERT',
				},
			],
		}

		var topic_arn_1 = 'topic_arn_1'
		var topic_arn_2 = 'topic_arn_2'

		routes = {
			modify: [
				{
					is: [
						{
							key: 'value',
						},
					],
					was: [
						{
							not: {
								key: 'old_value',
							},
						},
					],
					topics: [topic_arn_1],
				},
			],
			insert: [
				{
					name: 'insert route test 1',
					is: [
						{
							key: 'value',
						},
					],
					topics: [topic_arn_2],
				},
			],
		}

		await router.routeChanges(routes, sns, lambda, event)

		sns.publish.should.have.been.called
		sns.publish.should.have.been.calledWith(sinon.match.has('TopicArn', topic_arn_2))
	})

	it('should not send an sns message to insert route if matching criteria fails', async () => {
		var uuid = 'somelonguuidstringwhichfails'

		var event = {
			Records: [
				{
					dynamodb: {
						Keys: {
							uuid: {
								S: uuid,
							},
						},
						NewImage: {
							key: {
								S: 'value',
							},
							uuid: {
								S: uuid,
							},
						},
					},
					eventSourceARN: 'arn:aws:dynamodb:us-west-2:account-id:table/test/stream/2015-06-27T00:48:05.899',
					eventName: 'INSERT',
				},
			],
		}

		var topic_arn_1 = 'topic_arn_1'
		var topic_arn_2 = 'topic_arn_2'

		routes = {
			modify: [
				{
					is: [
						{
							key: 'value',
						},
						{
							undefined: ['not_defined'],
						},
					],
					was: [
						{
							not: {
								key: 'old_value',
							},
						},
					],
					topics: [topic_arn_1],
				},
			],
			insert: [
				{
					name: 'insert route test 2',
					is: [
						{
							key: 'different_value',
						},
					],
					topics: [topic_arn_2],
				},
			],
		}

		await router.routeChanges(routes, sns, lambda, event)

		sns.publish.should.not.have.been.called
	})

	it('should not send an sns message if insert routes are missing', async () => {
		var uuid = 'somelonguuidstring'

		var event = {
			Records: [
				{
					dynamodb: {
						Keys: {
							uuid: {
								S: uuid,
							},
						},
						NewImage: {
							key: {
								S: 'value',
							},
							uuid: {
								S: uuid,
							},
						},
					},
					eventSourceARN: 'arn:aws:dynamodb:us-west-2:account-id:table/test/stream/2015-06-27T00:48:05.899',
					eventName: 'INSERT',
				},
			],
		}

		var topic_arn_1 = 'topic_arn_1'

		routes = {
			modify: [
				{
					is: [
						{
							key: 'value',
						},
						{
							undefined: ['not_defined'],
						},
					],
					was: [
						{
							not: {
								key: 'old_value',
							},
						},
					],
					topics: [topic_arn_1],
				},
			],
		}

		await router.routeChanges(routes, sns, lambda, event)

		sns.publish.should.not.have.been.called
	})

	it('should send an sns message to modify route only matching the criteria', async () => {
		var uuid = 'somelonguuidstring'

		var event = {
			Records: [
				{
					dynamodb: {
						Keys: {
							uuid: {
								S: uuid,
							},
						},
						NewImage: {
							key: {
								S: 'value',
							},
							uuid: {
								S: uuid,
							},
							key_map_1: {
								M: {
									key_map_2: {
										M: {
											mapped_key: {
												S: 'some_value',
											},
										},
									},
								},
							},
						},
						OldImage: {
							key: {
								S: 'old_value',
							},
							uuid: {
								S: uuid,
							},
						},
					},
					eventSourceARN: 'arn:aws:dynamodb:us-west-2:account-id:table/test/stream/2015-06-27T00:48:05.899',
					eventName: 'MODIFY',
				},
			],
		}

		var topic_arn_1 = 'topic_arn_1'
		var topic_arn_2 = 'topic_arn_2'

		routes = {
			modify: [
				{
					name: 'modify route test 1',
					payload: 'key',
					is: [
						{
							key: 'value',
						},
						{
							defined: ['key_map_1.key_map_2.mapped_key'],
						},
					],
					was: [
						{
							not: {
								key: 'value',
							},
						},
						{
							undefined: ['key_map_1.key_map_2.mapped_key'],
						},
					],
					topics: [topic_arn_1],
				},
			],
			insert: [
				{
					is: [
						{
							key: 'value',
						},
					],
					topics: [topic_arn_2],
				},
			],
		}

		await router.routeChanges(routes, sns, lambda, event)

		sns.publish.should.have.been.called
		sns.publish.should.have.been.calledWith(sinon.match.has('TopicArn', topic_arn_1))
		sns.publish.should.have.been.calledWith(sinon.match.has('Message', 'value'))
	})

	it('should send an sns message to modify route only matching the criteria and send tableName', async () => {
		var uuid = 'somelonguuidstring'

		var event = {
			Records: [
				{
					dynamodb: {
						Keys: {
							uuid: {
								S: uuid,
							},
						},
						NewImage: {
							key: {
								S: 'value',
							},
							uuid: {
								S: uuid,
							},
							key_map_1: {
								M: {
									key_map_2: {
										M: {
											mapped_key: {
												S: 'some_value',
											},
										},
									},
								},
							},
						},
						OldImage: {
							key: {
								S: 'old_value',
							},
							uuid: {
								S: uuid,
							},
						},
					},
					eventSourceARN: 'arn:aws:dynamodb:us-west-2:account-id:table/test/stream/2015-06-27T00:48:05.899',
					eventName: 'MODIFY',
				},
			],
		}

		var topic_arn_1 = 'topic_arn_1'
		var topic_arn_2 = 'topic_arn_2'

		routes = {
			modify: [
				{
					name: 'modify route test 1',
					payload: 'key',
					includeTableName: true,
					is: [
						{
							key: 'value',
						},
						{
							defined: ['key_map_1.key_map_2.mapped_key'],
						},
					],
					was: [
						{
							not: {
								key: 'value',
							},
						},
						{
							undefined: ['key_map_1.key_map_2.mapped_key'],
						},
					],
					topics: [topic_arn_1],
				},
			],
			insert: [
				{
					is: [
						{
							key: 'value',
						},
					],
					topics: [topic_arn_2],
				},
			],
		}

		await router.routeChanges(routes, sns, lambda, event)

		sns.publish.should.have.been.called
		sns.publish.should.have.been.calledWith(sinon.match.has('TopicArn', topic_arn_1))
		sns.publish.should.have.been.calledWith(
			sinon.match.has(
				'Message',
				JSON.stringify({
					payload: 'value',
					context: {
						tableName: 'test',
					},
				})
			)
		)
	})

	it('should send an sns message with uuid only to modify route only matching the criteria where route payload is a key', async () => {
		var uuid = 'somelonguuidstring'

		var event = {
			Records: [
				{
					dynamodb: {
						Keys: {
							uuid: {
								S: uuid,
							},
						},
						NewImage: {
							key: {
								S: 'value',
							},
							uuid: {
								S: uuid,
							},
							key_map_1: {
								M: {
									key_map_2: {
										M: {
											mapped_key: {
												S: 'another_value',
											},
										},
									},
								},
							},
						},
						OldImage: {
							key: {
								S: 'encoding',
							},
							uuid: {
								S: uuid,
							},
						},
					},
					eventSourceARN: 'arn:aws:dynamodb:us-west-2:account-id:table/test/stream/2015-06-27T00:48:05.899',
					eventName: 'MODIFY',
				},
			],
		}

		var topic_arn_1 = 'topic_arn_1'
		var topic_arn_2 = 'topic_arn_2'

		routes = {
			modify: [
				{
					name: 'Test rule with uuid as payload',
					payload: 'uuid',
					is: [
						{
							key: 'value',
						},
						{
							defined: ['key_map_1.key_map_2.mapped_key'],
						},
					],
					was: [
						{
							not: {
								key: 'value',
							},
						},
						{
							undefined: ['key_map_1.key_map_2.mapped_key'],
						},
					],
					topics: [topic_arn_1],
				},
			],
			insert: [
				{
					is: [
						{
							key: 'old_value',
						},
					],
					topics: [topic_arn_2],
				},
			],
		}

		await router.routeChanges(routes, sns, lambda, event)

		sns.publish.should.have.been.calledWith(sinon.match.has('TopicArn', topic_arn_1))
		sns.publish.should.have.been.calledWith(sinon.match.has('Message', uuid))
	})

	it('should send an sns message with uuid only to modify route including an is with undefined only matching the criteria where route payload is uuid', async () => {
		var uuid = 'somelonguuidstring'

		var event = {
			Records: [
				{
					dynamodb: {
						Keys: {
							uuid: {
								S: uuid,
							},
						},
						NewImage: {
							key: {
								S: 'value',
							},
							uuid: {
								S: uuid,
							},
							defined_key: {
								S: 'defined_value',
							},
							another_key: {
								S: 'test',
							},
							key_map_1: {
								M: {
									key_map_2: {
										M: {
											mapped_key: {
												S: 'another_value',
											},
										},
									},
								},
							},
						},
						OldImage: {
							key: {
								S: 'encoding',
							},
							another_key: {
								S: 'test',
							},
							uuid: {
								S: uuid,
							},
						},
					},
					eventSourceARN: 'arn:aws:dynamodb:us-west-2:account-id:table/test/stream/2015-06-27T00:48:05.899',
					eventName: 'MODIFY',
				},
			],
		}

		var topic_arn_1 = 'topic_arn_1'
		var topic_arn_2 = 'topic_arn_2'

		routes = {
			modify: [
				{
					name: 'Test rule with uuid as payload without defined_key',
					payload: 'uuid',
					is: [
						{
							key: 'value',
							another_key: 'test',
						},
						{
							undefined: ['defined_key'],
						},
					],
					was: [
						{
							not: {
								key: 'value',
							},
						},
						{
							another_key: 'test',
						},
					],
					topics: [topic_arn_1],
				},
				{
					name: 'Test rule with uuid as payload with defined_key',
					payload: 'uuid',
					is: [
						{
							key: 'value',
							another_key: 'test',
						},
						{
							defined: ['defined_key'],
						},
					],
					was: [
						{
							not: {
								key: 'value',
							},
						},
						{
							another_key: 'test',
						},
					],
					topics: [topic_arn_2],
				},
			],
			insert: [],
		}

		await router.routeChanges(routes, sns, lambda, event)

		sns.publish.should.have.been.calledWith(sinon.match.has('TopicArn', topic_arn_2))
		sns.publish.should.have.been.calledWith(sinon.match.has('Message', uuid))
	})

	it('should send an sns message to modify route only matching the criteria with defined in an array', async () => {
		var uuid = 'somelonguuidstring'

		var event = {
			Records: [
				{
					dynamodb: {
						Keys: {
							uuid: {
								S: uuid,
							},
						},
						NewImage: {
							key: {
								S: 'value',
							},
							uuid: {
								S: uuid,
							},
							array_key: {
								L: [
									{
										M: {
											key_1: {
												S: 'key_1_value',
											},
											key_2: {
												S: 'key_2_value',
											},
											key_3: {
												S: 'key_3_value',
											},
											key_4: {
												S: 'key_4_value',
											},
										},
									},
								],
							},
						},
						OldImage: {
							key: {
								S: 'value',
							},
							uuid: {
								S: uuid,
							},
							array_key: {
								L: [
									{
										M: {
											key_1: {
												S: 'key_1_value',
											},
											key_2: {
												S: 'key_2_value',
											},
											key_3: {
												S: 'key_3_value',
											},
										},
									},
								],
							},
						},
					},
					eventSourceARN: 'arn:aws:dynamodb:us-west-2:account-id:table/test/stream/2015-06-27T00:48:05.899',
					eventName: 'MODIFY',
				},
			],
		}

		var topic_arn_1 = 'topic_arn_1'
		var topic_arn_2 = 'topic_arn_2'

		routes = {
			modify: [
				{
					name: 'New record has key_4 defined in an obj inside an array where old record did not where key_1 has a matching value',
					is: [
						{
							key: 'value',
						},
						{
							matches: [
								function(record) {
									return (
										_.find(record.array_key, function(obj) {
											return obj.key_1 == 'key_1_value' && obj.key_4 !== undefined
										}) !== undefined
									)
								},
							],
						},
					],
					was: [
						{
							key: 'value',
						},
						{
							matches: [
								function(record) {
									return (
										_.find(record.array_key, function(obj) {
											return obj.key_1 == 'key_1_value' && obj.key_4
										}) === undefined
									)
								},
							],
						},
					],
					topics: [topic_arn_1],
				},
			],
			insert: [
				{
					is: [
						{
							key: 'old_value',
						},
					],
					topics: [topic_arn_2],
				},
			],
		}

		await router.routeChanges(routes, sns, lambda, event)

		sns.publish.should.have.been.calledWith(sinon.match.has('TopicArn', topic_arn_1))
	})

	it('should send an sns message to modify route only matching the criteria including a function', async () => {
		var uuid = 'somelonguuidstring'

		var event = {
			Records: [
				{
					dynamodb: {
						Keys: {
							uuid: {
								S: uuid,
							},
						},
						NewImage: {
							defined_key: {
								S: 'defined_value',
							},
							key: {
								S: 'value',
							},
							uuid: {
								S: uuid,
							},
							array_key: {
								L: [
									{
										M: {
											key_1: {
												S: 'key_1_value_1',
											},
											key_2: {
												S: 'key_2_value',
											},
											key_3: {
												S: 'key_3_value',
											},
											key_4: {
												S: 'key_4_value',
											},
										},
									},
									{
										M: {
											key_1: {
												S: 'key_1_value_2',
											},
											key_2: {
												S: 'key_2_value',
											},
											key_3: {
												S: 'key_3_value',
											},
											key_4: {
												S: 'key_4_value',
											},
										},
									},
								],
							},
						},
						OldImage: {
							defined_key: {
								S: 'defined_value',
							},
							key: {
								S: 'value',
							},
							uuid: {
								S: uuid,
							},
							array_key: {
								L: [
									{
										M: {
											key_1: {
												S: 'key_1_value_1',
											},
											key_2: {
												S: 'key_2_value',
											},
											key_3: {
												S: 'key_3_value',
											},
										},
									},
									{
										M: {
											key_1: {
												S: 'key_1_value_2',
											},
											key_2: {
												S: 'key_2_value',
											},
											key_3: {
												S: 'key_3_value',
											},
											key_4: {
												S: 'key_4_value',
											},
										},
									},
								],
							},
						},
					},
					eventSourceARN: 'arn:aws:dynamodb:us-west-2:account-id:table/test/stream/2015-06-27T00:48:05.899',
					eventName: 'MODIFY',
				},
			],
		}

		var topic_arn_1 = 'topic_arn_1'
		var topic_arn_2 = 'topic_arn_2'

		routes = {
			modify: [
				{
					name: 'New record has key_4 defined in an obj inside an array where old record did not',
					is: [
						{
							key: 'value',
							defined_key: 'defined_value',
						},
						{
							matches: [
								function(record) {
									return _.every(record.array_key, function(obj) {
										return obj.key_4
									})
								},
							],
						},
					],
					was: [
						{
							key: 'value',
							defined_key: 'defined_value',
						},
						{
							matches: [
								function(record) {
									return !_.every(record.array_key, function(obj) {
										return obj.key_4
									})
								},
							],
						},
					],
					topics: [topic_arn_1],
				},
			],
			insert: [
				{
					is: [
						{
							key: 'value',
						},
					],
					topics: [topic_arn_2],
				},
			],
		}

		await router.routeChanges(routes, sns, lambda, event)

		sns.publish.should.have.been.calledWith(sinon.match.has('TopicArn', topic_arn_1))
	})

	it('should send an sns message to the matching modify route using a changed rule', async () => {
		var uuid = 'somelonguuidstring'

		var event = {
			Records: [
				{
					dynamodb: {
						Keys: {
							uuid: {
								S: uuid,
							},
						},
						NewImage: {
							key: {
								S: 'new_value',
							},
							uuid: {
								S: uuid,
							},
						},
						OldImage: {
							key: {
								S: 'old_value',
							},
							uuid: {
								S: uuid,
							},
						},
					},
					eventSourceARN: 'arn:aws:dynamodb:us-west-2:account-id:table/test/stream/2015-06-27T00:48:05.899',
					eventName: 'MODIFY',
				},
			],
		}

		var topic_arn_1 = 'topic_arn_1'

		routes = {
			modify: [
				{
					changed: ['key'],
					topics: [topic_arn_1],
				},
			],
		}

		await router.routeChanges(routes, sns, lambda, event)

		sns.publish.should.have.been.calledWith(sinon.match.has('TopicArn', topic_arn_1))
	})

	it('should send an sns message to the remove route only matching the criteria', async () => {
		var uuid = 'somelonguuidstring'

		var event = {
			Records: [
				{
					dynamodb: {
						Keys: {
							uuid: {
								S: uuid,
							},
						},
						OldImage: {
							key: {
								S: 'value',
							},
							uuid: {
								S: uuid,
							},
						},
					},
					eventSourceARN: 'arn:aws:dynamodb:us-west-2:account-id:table/test/stream/2015-06-27T00:48:05.899',
					eventName: 'REMOVE',
				},
			],
		}

		var topic_arn_1 = 'topic_arn_1'
		var topic_arn_2 = 'topic_arn_2'
		var topic_arn_3 = 'topic_arn_3'

		routes = {
			modify: [
				{
					is: [
						{
							key: 'value',
						},
					],
					was: [
						{
							not: {
								key: 'old_value',
							},
						},
					],
					topics: [topic_arn_1],
				},
			],
			insert: [
				{
					name: 'remove route test 1 - insert',
					is: [
						{
							key: 'value',
						},
					],
					topics: [topic_arn_2],
				},
			],
			remove: [
				{
					name: 'remove route test 1 - remove',
					was: [
						{
							key: 'value',
						},
					],
					topics: [topic_arn_3],
				},
			],
		}

		await router.routeChanges(routes, sns, lambda, event)

		sns.publish.should.have.been.calledWith(sinon.match.has('TopicArn', topic_arn_3))
	})

	it('should invoke the lambda function from insert route only matching the criteria', async () => {
		var uuid = 'somelonguuidstring'

		var event = {
			Records: [
				{
					dynamodb: {
						Keys: {
							uuid: {
								S: uuid,
							},
						},
						NewImage: {
							key: {
								S: 'value',
							},
							uuid: {
								S: uuid,
							},
						},
					},
					eventSourceARN: 'arn:aws:dynamodb:us-west-2:account-id:table/test/stream/2015-06-27T00:48:05.899',
					eventName: 'INSERT',
				},
			],
		}

		var lambda_arn_1 = 'lambda_arn_1'
		var lambda_arn_2 = 'lambda_arn_2'

		routes = {
			modify: [
				{
					is: [
						{
							key: 'value',
						},
					],
					was: [
						{
							not: {
								key: 'old_value',
							},
						},
					],
					lambdas: [lambda_arn_1],
				},
			],
			insert: [
				{
					name: 'insert route test 1',
					qualifier: 'environment',
					is: [
						{
							key: 'value',
						},
					],
					lambdas: [lambda_arn_2],
				},
			],
		}

		await router.routeChanges(routes, sns, lambda, event)

		lambda.invoke.should.have.been.calledWith(sinon.match.has('FunctionName', lambda_arn_2))
		lambda.invoke.should.not.have.been.calledWith(sinon.match.has('Qualifier', 'prod'))
	})

	it('should invoke the lambda function from modify route only matching the criteria with a qualifier', async () => {
		var uuid = 'somelonguuidstring'
		var environment = 'test'

		var event = {
			Records: [
				{
					dynamodb: {
						Keys: {
							uuid: {
								S: uuid,
							},
						},
						NewImage: {
							key: {
								S: 'value',
							},
							uuid: {
								S: uuid,
							},
							key_map_1: {
								M: {
									key_map_2: {
										M: {
											mapped_key: {
												S: 'some_value',
											},
										},
									},
								},
							},
							environment: {
								S: environment,
							},
						},
						OldImage: {
							key: {
								S: 'old_value',
							},
							uuid: {
								S: uuid,
							},
							environment: {
								S: environment,
							},
						},
					},
					eventSourceARN: 'arn:aws:dynamodb:us-west-2:account-id:table/test/stream/2015-06-27T00:48:05.899',
					eventName: 'MODIFY',
				},
			],
		}

		var lambda_arn_1 = 'lambda_arn_1'
		var lambda_arn_2 = 'lambda_arn_2'

		routes = {
			modify: [
				{
					name: 'modify route test 1',
					payload: 'key',
					qualifier: 'environment',
					is: [
						{
							key: 'value',
						},
						{
							defined: ['key_map_1.key_map_2.mapped_key'],
						},
					],
					was: [
						{
							not: {
								key: 'value',
							},
						},
						{
							undefined: ['key_map_1.key_map_2.mapped_key'],
						},
					],
					lambdas: [lambda_arn_1],
				},
			],
			insert: [
				{
					is: [
						{
							key: 'value',
						},
					],
					lambdas: [lambda_arn_2],
				},
			],
		}

		await router.routeChanges(routes, sns, lambda, event)

		lambda.invoke.should.have.been.calledWith(sinon.match.has('FunctionName', lambda_arn_1))
		lambda.invoke.should.have.been.calledWith(sinon.match.has('Qualifier', environment))
	})

	it('should invoke the lambda function from modify route only matching the criteria with a prefix', async () => {
		var uuid = 'somelonguuidstring'
		var environment = 'test'

		var event = {
			Records: [
				{
					dynamodb: {
						Keys: {
							uuid: {
								S: uuid,
							},
						},
						NewImage: {
							key: {
								S: 'value',
							},
							uuid: {
								S: uuid,
							},
							key_map_1: {
								M: {
									key_map_2: {
										M: {
											mapped_key: {
												S: 'some_value',
											},
										},
									},
								},
							},
							environment: {
								S: environment,
							},
						},
						OldImage: {
							key: {
								S: 'old_value',
							},
							uuid: {
								S: uuid,
							},
							environment: {
								S: environment,
							},
						},
					},
					eventSourceARN: 'arn:aws:dynamodb:us-west-2:account-id:table/test/stream/2015-06-27T00:48:05.899',
					eventName: 'MODIFY',
				},
			],
		}

		var lambda_arn_1 = 'arn:aws:lambda:us-west-2:000000000000:function:functionName1'
		var lambda_arn_2 = 'arn:aws:lambda:us-west-2:000000000000:function:functionName2'

		routes = {
			modify: [
				{
					name: 'modify route test 1',
					payload: 'key',
					qualifier: 'environment',
					prefix: ['environment', 'default'],
					is: [
						{
							key: 'value',
						},
						{
							defined: ['key_map_1.key_map_2.mapped_key'],
						},
					],
					was: [
						{
							not: {
								key: 'value',
							},
						},
						{
							undefined: ['key_map_1.key_map_2.mapped_key'],
						},
					],
					lambdas: [lambda_arn_1],
				},
			],
			insert: [
				{
					is: [
						{
							key: 'value',
						},
					],
					lambdas: [lambda_arn_2],
				},
			],
		}

		await router.routeChanges(routes, sns, lambda, event)

		lambda.invoke.should.have.been.calledWith(sinon.match.has('FunctionName', 'arn:aws:lambda:us-west-2:000000000000:function:test-functionName1'))
		lambda.invoke.should.have.been.calledWith(sinon.match.has('Qualifier', environment))
	})

	it('should invoke the lambda function from modify route only matching the criteria with a default prefix', async () => {
		var uuid = 'somelonguuidstring'

		var event = {
			Records: [
				{
					dynamodb: {
						Keys: {
							uuid: {
								S: uuid,
							},
						},
						NewImage: {
							key: {
								S: 'value',
							},
							uuid: {
								S: uuid,
							},
							key_map_1: {
								M: {
									key_map_2: {
										M: {
											mapped_key: {
												S: 'some_value',
											},
										},
									},
								},
							},
						},
						OldImage: {
							key: {
								S: 'old_value',
							},
							uuid: {
								S: uuid,
							},
						},
					},
					eventSourceARN: 'arn:aws:dynamodb:us-west-2:account-id:table/test/stream/2015-06-27T00:48:05.899',
					eventName: 'MODIFY',
				},
			],
		}

		var lambda_arn_1 = 'arn:aws:lambda:us-west-2:000000000000:function:functionName1'
		var lambda_arn_2 = 'arn:aws:lambda:us-west-2:000000000000:function:functionName2'

		routes = {
			modify: [
				{
					name: 'modify route test 1',
					payload: 'key',
					prefix: ['environment', 'default'],
					is: [
						{
							key: 'value',
						},
						{
							defined: ['key_map_1.key_map_2.mapped_key'],
						},
					],
					was: [
						{
							not: {
								key: 'value',
							},
						},
						{
							undefined: ['key_map_1.key_map_2.mapped_key'],
						},
					],
					lambdas: [lambda_arn_1],
				},
			],
			insert: [
				{
					is: [
						{
							key: 'value',
						},
					],
					lambdas: [lambda_arn_2],
				},
			],
		}

		await router.routeChanges(routes, sns, lambda, event)

		lambda.invoke.should.have.been.calledWith(sinon.match.has('FunctionName', 'arn:aws:lambda:us-west-2:000000000000:function:default-functionName1'))
	})

	it('should invoke the lambda function from modify route only matching the criteria with an array of values', async () => {
		var uuid = 'somelonguuidstring'
		var status = 'new'
		var event = {
			Records: [
				{
					dynamodb: {
						Keys: {
							uuid: {
								S: uuid,
							},
						},
						NewImage: {
							key: {
								S: 'value',
							},
							uuid: {
								S: uuid,
							},
							status: {
								S: status,
							},
						},
						OldImage: {
							key: {
								S: 'old_value',
							},
							uuid: {
								S: uuid,
							},
						},
					},
					eventSourceARN: 'arn:aws:dynamodb:us-west-2:account-id:table/test/stream/2015-06-27T00:48:05.899',
					eventName: 'MODIFY',
				},
			],
		}

		var lambda_arn_1 = 'lambda_arn_1'

		routes = {
			modify: [
				{
					name: 'modify route test 1',
					is: [
						{
							key: 'value',
						},
						{
							status: ['new', 'in_progress', 'done'],
						},
					],
					was: [
						{
							not: {
								key: ['value', 'some_other_value'],
							},
						},
					],
					lambdas: [lambda_arn_1],
				},
			],
		}

		await router.routeChanges(routes, sns, lambda, event)

		lambda.invoke.should.have.been.calledWith(sinon.match.has('FunctionName', lambda_arn_1))
	})

	it('should invoke the lambda function from modify route using the old record', async () => {
		var uuid = 'somelonguuidstring'
		var status = 'old'
		var event = {
			Records: [
				{
					dynamodb: {
						Keys: {
							uuid: {
								S: uuid,
							},
						},
						NewImage: {
							key: {
								S: 'value',
							},
							uuid: {
								S: uuid,
							},
							status: {
								S: 'new',
							},
						},
						OldImage: {
							key: {
								S: 'old_value',
							},
							uuid: {
								S: uuid,
							},
							status: {
								S: status,
							},
						},
					},
					eventSourceARN: 'arn:aws:dynamodb:us-west-2:account-id:table/test/stream/2015-06-27T00:48:05.899',
					eventName: 'MODIFY',
				},
			],
		}

		var lambda_arn_1 = 'lambda_arn_1'

		routes = {
			modify: [
				{
					name: 'modify route test 1',
					useOldRecord: true,
					is: [
						{
							key: 'value',
						},
						{
							status: ['new', 'in_progress', 'done'],
						},
					],
					was: [
						{
							status: 'old',
						},
					],
					lambdas: [lambda_arn_1],
				},
			],
		}

		await router.routeChanges(routes, sns, lambda, event)

		lambda.invoke.should.have.been.calledWith(sinon.match.has('FunctionName', lambda_arn_1))
		lambda.invoke.should.have.been.calledWith(
			sinon.match.has(
				'Payload',
				JSON.stringify({
					key: 'old_value',
					uuid: uuid,
					status: status,
				})
			)
		)
	})

	it('should invoke the lambda function from modify route only matching the criteria with a qualifier, and send tableName with the payload', async () => {
		var uuid = 'somelonguuidstring'
		var environment = 'test'

		var event = {
			Records: [
				{
					dynamodb: {
						Keys: {
							uuid: {
								S: uuid,
							},
						},
						NewImage: {
							key: {
								S: 'value',
							},
							uuid: {
								S: uuid,
							},
							key_map_1: {
								M: {
									key_map_2: {
										M: {
											mapped_key: {
												S: 'some_value',
											},
										},
									},
								},
							},
							environment: {
								S: environment,
							},
						},
						OldImage: {
							key: {
								S: 'old_value',
							},
							uuid: {
								S: uuid,
							},
							environment: {
								S: environment,
							},
						},
					},
					eventSourceARN: 'arn:aws:dynamodb:us-west-2:account-id:table/test/stream/2015-06-27T00:48:05.899',
					eventName: 'MODIFY',
				},
			],
		}

		var lambda_arn_1 = 'lambda_arn_1'
		var lambda_arn_2 = 'lambda_arn_2'

		routes = {
			modify: [
				{
					name: 'modify route test 1',
					payload: 'key',
					qualifier: 'environment',
					includeTableName: true,
					is: [
						{
							key: 'value',
						},
						{
							defined: ['key_map_1.key_map_2.mapped_key'],
						},
					],
					was: [
						{
							not: {
								key: 'value',
							},
						},
						{
							undefined: ['key_map_1.key_map_2.mapped_key'],
						},
					],
					lambdas: [lambda_arn_1],
				},
			],
			insert: [
				{
					is: [
						{
							key: 'value',
						},
					],
					lambdas: [lambda_arn_2],
				},
			],
		}

		await router.routeChanges(routes, sns, lambda, event)

		lambda.invoke.should.have.been.calledWith(sinon.match.has('FunctionName', lambda_arn_1))
		lambda.invoke.should.have.been.calledWith(sinon.match.has('Qualifier', environment))
		lambda.invoke.should.have.been.calledWith(
			sinon.match.has(
				'Payload',
				JSON.stringify({
					payload: 'value',
					context: {
						tableName: 'test',
					},
				})
			)
		)
	})

	it('should invoke multiple matching routes', async () => {
		var uuid = 'somelonguuidstring'
		var environment = 'test'

		var event = {
			Records: [
				{
					dynamodb: {
						Keys: {
							uuid: {
								S: uuid,
							},
						},
						NewImage: {
							key: {
								S: 'value',
							},
							uuid: {
								S: uuid,
							},
							key_map_1: {
								M: {
									key_map_2: {
										M: {
											mapped_key: {
												S: 'some_value',
											},
											another_mapped_key: {
												S: 'some_other_value',
											},
										},
									},
								},
							},
							environment: {
								S: environment,
							},
						},
						OldImage: {
							key: {
								S: 'old_value',
							},
							uuid: {
								S: uuid,
							},
							environment: {
								S: environment,
							},
						},
					},
					eventSourceARN: 'arn:aws:dynamodb:us-west-2:account-id:table/test/stream/2015-06-27T00:48:05.899',
					eventName: 'MODIFY',
				},
			],
		}

		var lambda_arn_1 = 'lambda_arn_1'
		var lambda_arn_2 = 'lambda_arn_2'
		var lambda_arn_3 = 'lambda_arn_3'

		routes = {
			modify: [
				{
					name: 'modify route test 1',
					payload: 'key',
					qualifier: 'environment',
					includeTableName: true,
					is: [
						{
							key: 'value',
						},
						{
							defined: ['key_map_1.key_map_2.mapped_key'],
						},
					],
					was: [
						{
							not: {
								key: 'value',
							},
						},
						{
							undefined: ['key_map_1.key_map_2.mapped_key'],
						},
					],
					lambdas: [lambda_arn_1],
				},
				{
					name: 'modify route test 2',
					payload: 'key',
					qualifier: 'environment',
					includeTableName: true,
					is: [
						{
							key: 'value',
						},
						{
							defined: ['key_map_1.key_map_2.another_mapped_key'],
						},
					],
					was: [
						{
							not: {
								key: 'value',
							},
						},
						{
							undefined: ['key_map_1.key_map_2.another_mapped_key'],
						},
					],
					lambdas: [lambda_arn_3],
				},
			],
			insert: [
				{
					is: [
						{
							key: 'value',
						},
					],
					lambdas: [lambda_arn_2],
				},
			],
		}

		await router.routeChanges(routes, sns, lambda, event)

		lambda.invoke.should.have.been.calledTwice
		lambda.invoke.should.have.been.calledWith(sinon.match.has('FunctionName', lambda_arn_1))
		lambda.invoke.should.have.been.calledWith(sinon.match.has('FunctionName', lambda_arn_3))
		lambda.invoke.should.have.been.calledWith(sinon.match.has('Qualifier', environment))
		lambda.invoke.should.have.been.calledWith(
			sinon.match.has(
				'Payload',
				JSON.stringify({
					payload: 'value',
					context: {
						tableName: 'test',
					},
				})
			)
		)
	})
})
