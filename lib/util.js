function normaliseRecord($record) {
    if (!$record)
        return null

    var normal = {}
    for (var key in $record) {
        if ($record.hasOwnProperty(key)) {
            if ($record[key].hasOwnProperty('S'))
                normal[key] = $record[key]['S']

            if ($record[key].hasOwnProperty('N'))
                normal[key] = +($record[key]['N'])

            if ($record[key].hasOwnProperty('BOOL'))
                normal[key] = $record[key]['BOOL']

            if ($record[key].hasOwnProperty('NULL'))
                normal[key] = null

            if ($record[key].hasOwnProperty('L')) {
                normal[key] = []
                for (var i in $record[key]['L']) {
                    if ($record[key]['L'].hasOwnProperty(i)) {
                        normal[key][i] = normaliseRecord({
                            key: $record[key]['L'][i]
                        }).key
                    }
                }
                normal[key]
            }

            if ($record[key].hasOwnProperty('M')) {
                normal[key] = {}
                for (var i in $record[key]['M']) {
                    if ($record[key]['M'].hasOwnProperty(i)) {
                        normal[key][i] = normaliseRecord({
                            key: $record[key]['M'][i]
                        }).key
                    }
                }
            }
        }
    }
    return normal;
}

module.exports = {
    normaliseRecord: normaliseRecord
};