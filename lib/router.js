const util = require('./util')
const _ = require('lodash')

let routes, sns, lambda

const addPrefixToLambdaName = (obj, lambdaArn, input) => {
	if (input) {
		var prefix = ''
		var prefixDefault = ''

		typeof input === 'string' && (prefix = obj[input] || '')

		if (Array.isArray(input)) {
			var length = input.length
			length === 1 && (prefix = obj[input[0]])
			length === 2 && (prefix = obj[input[0]])
			length === 2 && (prefixDefault = input[1])
		}

		prefix && (prefix = prefix + '-')
		prefixDefault && (prefixDefault = prefixDefault + '-')

		var resultArray = lambdaArn.split(':')
		resultArray[6] = (prefix ? prefix : prefixDefault) + resultArray[6]

		return resultArray.join(':')
	} else {
		return lambdaArn
	}
}

const functionsEvaluateAsTrue = (obj, criteria) => {
	return criteria.every(criterion => {
		if (typeof criterion == 'function') {
			return criterion.call(criterion, obj)
		} else {
			return false //Not a function
		}
	})
}

const keysAreUndefined = (obj, criteria) => {
	return criteria.every(criterion => {
		return _.get(obj, criterion) === undefined
	})
}

const keysAreDefined = (obj, criteria) => {
	return criteria.every(criterion => {
		return _.get(obj, criterion) !== undefined
	})
}

const keysNotMatch = (obj, criteria) => {
	return Object.keys(criteria).every(criterion => {
		if (_.isArray(_.get(criteria, criterion))) {
			return !_.includes(_.get(criteria, criterion), _.get(obj, criterion))
		} else {
			return _.get(obj, criterion) !== _.get(criteria, criterion)
		}
	})
}

const keysMatch = (obj, criteria) => {
	return Object.keys(criteria).every(criterion => {
		if (_.isArray(_.get(criteria, criterion))) {
			return _.includes(_.get(criteria, criterion), _.get(obj, criterion))
		} else {
			return _.get(obj, criterion) === _.get(criteria, criterion)
		}
	})
}

const evaluateCriteria = (obj, criteria) => {
	if (criteria.not) {
		return keysNotMatch(obj, criteria.not)
	} else if (criteria.defined) {
		return keysAreDefined(obj, criteria.defined)
	} else if (criteria.undefined) {
		return keysAreUndefined(obj, criteria.undefined)
	} else if (criteria.matches) {
		return functionsEvaluateAsTrue(obj, criteria.matches)
	} else {
		return keysMatch(obj, criteria)
	}
}

const createPayload = (obj, route, tableName) => {
	var payload = route.payload && obj[route.payload] ? obj[route.payload] : obj
	if (route.includeTableName) {
		return JSON.stringify({
			payload: payload,
			context: {
				tableName: tableName,
			},
		})
	} else {
		try {
			return typeof payload === 'string' ? payload : JSON.stringify(payload)
		} catch (exception) {
			return payload
		}
	}
}

const sendPayloadToTopic = async (tableName, obj, topic, route) => {
	console.log('Sending payload to topic: ' + topic)
	console.log(createPayload(obj, route, tableName))
	await sns
		.publish({
			Message: createPayload(obj, route, tableName),
			TopicArn: topic,
		})
		.promise()
}

const invokeLambdaWithPayload = async (tableName, obj, functionName, route) => {
	console.log('Using payload to call lambda: ' + functionName)
	var params = {
		FunctionName: addPrefixToLambdaName(obj, functionName, route.prefix),
		InvocationType: 'Event',
		Payload: createPayload(obj, route, tableName),
	}
	if (route.qualifier && obj[route.qualifier]) {
		params.Qualifier = obj[route.qualifier]
	}
	await lambda.invoke(params).promise()
}

const processRoute = async (tableName, record, route) => {
	if (typeof route.lambdas !== 'undefined') {
		await Promise.all(
			route.lambdas.map(async functionName => {
				await invokeLambdaWithPayload(tableName, record, functionName, route)
			})
		)
	}
	if (typeof route.topics !== 'undefined') {
		await Promise.all(
			route.topics.map(async topic => {
				await sendPayloadToTopic(tableName, record, topic, route)
			})
		)
	}
}

const routeInsert = async (tableName, newRecord) => {
	await Promise.all(
		_.get(routes, 'insert', []).map(async route => {
			if (
				_.get(route, 'is', []).every(criteria => {
					return evaluateCriteria(newRecord, criteria)
				})
			) {
				console.log('Insert rule ' + route.name + ' triggered')
				await processRoute(tableName, newRecord, route)
			}
		})
	)
}

const routeModify = async (tableName, newRecord, oldRecord) => {
	await Promise.all(
		_.get(routes, 'modify', []).map(async route => {
			if (
				_.get(route, 'is', []).every(criteria => {
					return evaluateCriteria(newRecord, criteria)
				}) &&
				_.get(route, 'was', []).every(criteria => {
					return evaluateCriteria(oldRecord, criteria)
				}) &&
				_.get(route, 'changed', []).every(criteria => {
					return _.get(newRecord, criteria) != _.get(oldRecord, criteria)
				})
			) {
				// Add 'changed' as a comparison type?
				console.log('Modify rule ' + route.name + ' triggered')
				var record = newRecord
				if (route.useOldRecord || route.useRecord === 'old') {
					record = oldRecord
				} else if (route.useRecord === 'both') {
					record = {
						new: newRecord,
						old: oldRecord,
					}
				}
				await processRoute(tableName, record, route)
			}
		})
	)
}

const routeRemove = async (tableName, oldRecord) => {
	await Promise.all(
		_.get(routes, 'remove', []).map(async route => {
			if (
				_.get(route, 'was', []).every(criteria => {
					return evaluateCriteria(oldRecord, criteria)
				})
			) {
				console.log('Remove rule ' + route.name + ' triggered')
				await processRoute(tableName, oldRecord, route)
			}
		})
	)
}

function getTableName(arn) {
	return arn.split('/')[1]
}

const routeChange = async ($routes, $sns, $lambda, record) => {
	routes = $routes
	sns = $sns
	lambda = $lambda
	var eventName = record.eventName.toLowerCase()
	switch (eventName) {
		case 'insert':
			await routeInsert(getTableName(record.eventSourceARN), util.normaliseRecord(record.dynamodb.NewImage))
			break
		case 'modify':
			await routeModify(
				getTableName(record.eventSourceARN),
				util.normaliseRecord(record.dynamodb.NewImage),
				util.normaliseRecord(record.dynamodb.OldImage)
			)
			break
		case 'remove':
			await routeRemove(getTableName(record.eventSourceARN), util.normaliseRecord(record.dynamodb.OldImage))
			break
		default:
			throw new Error('Unable to handle event: ' + eventName)
	}
}

const routeChanges = async ($routes, $sns, $lambda, event) => {
	routes = $routes
	sns = $sns
	lambda = $lambda
	const failedRecords = []
	await Promise.all(
		event.Records.map(async record => {
			try {
				await routeChange($routes, $sns, $lambda, record)
			} catch (err) {
				console.log(err)
				failedRecords.push(record)
			}
		})
	)
	return failedRecords
}

module.exports = {
	routeChange: routeChange,
	routeChanges: routeChanges,
}
